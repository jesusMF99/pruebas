package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class Controlador implements Initializable{

	@FXML ToggleGroup funcion;
	@FXML ImageView imgFuncion;
	@FXML RadioButton rbOn;
	@FXML RadioButton rbOf;
	@FXML AnchorPane contenedor;


	String seleccion;



	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub

	}

	public void cargarImagen(String imagen){
		try{
			Image img=new Image("file:src/application/img/" + imagen);
			imgFuncion.setImage(img);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML public void seleccionOn(){

		cargarImagen("on.gif");
		contenedor.setStyle("-fx-background-color: #e8e86a");

		}
	@FXML public void seleccionOf(){
		cargarImagen("off.gif");
		contenedor.setStyle("-fx-background-color: #44403f");

	}


}
